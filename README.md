# App Ranker

### Node Server: 3000

### React Server: 3001
1. Go to [package.json](package.json)
2. Change `scripts` as:  
* For Windows
```js
"start": "set PORT=3001 && react-scripts start"
```              
* For Linux
```js
"start":"PORT 3001 react-scripts start"
```

# Screenshots

 1. Mainform
<img src="screenshots/MainForm.png">

2. Leaderboard
<img src="screenshots/Leaderboard.png">

3. Search Record Not Found
<img src="screenshots/searchNotFound.png">

4. App Details
<img src="screenshots/appDetails.png">