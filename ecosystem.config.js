module.exports = {
  apps: [{
    name: 'RankingApp',
    script: 'npm',

    // Options reference: https://pm2.keymetrics.io/docs/usage/application-declaration/
    args: 'one two',
    instances: 1,
    autorestart: true,
    watch: false,
    max_memory_restart: '1G',
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy: {
    production: {
      user: 'root',
      host: '68.183.89.242',
      ref: 'origin/master',
      repo: 'https://gitlab.com/tacmoktan/rankingapp.git',
      path: '/var/www/rankingapp',
      'post-deploy': 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
