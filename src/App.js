import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
//pages
import Home from './components/Home';
import AppDetails from './components/AppDetails';
//styles
import './css/styles.css';

function App() {

		return (
			<BrowserRouter>
				<div className="App">

					<Switch>
						<Route exact path='/' component={Home} />
						<Route path='/app-details/:appId' component ={AppDetails} />
					</Switch>
				</div>
			</BrowserRouter>
		);
}

export default App;
