import React from 'react';
import styled from 'styled-components';

let ToggleButton = styled.div`
    border: none;
    background: ${props => props.lineBackground};
    color: white;
    border-radius: 20px;
    cursor: pointer;
    outline: none;
    position: relative;
    width: 60px;
    height: 15px;
    transition: 0.3s ease-in;

    &:after{
      content: "";
      display: block;
      position: absolute;
      width: 25px;
      height: 25px;
      background: ${props => props.circleBackground};
      border-radius: 50%;
      top: -5px;
      left: 0;
      transition: 0.2s ease-in;
      transform: ${props => props.transform};
      box-shadow:  ${props => props.boxShadow};
    }
`

export default ({ handleShowCategoryField, styles }) => {


  return (
    <div className="toggle-btn-container">
      <h3 className="category-title">Category</h3>
      <ToggleButton lineBackground={styles.lineBackground} circleBackground={styles.circleBackground} transform={styles.transform} boxShadow={styles.boxShadow} onClick={() => handleShowCategoryField()}>
      </ToggleButton>
    </div>
  )
}