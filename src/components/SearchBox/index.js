import React from 'react';

export default ({ searchInput, handleEvent, handleSearch }) =>
    (
        <form className="search-box">
            <input type="text" className="search-field" value={searchInput} onChange={handleEvent('searchInput')} placeholder="Search..." />
            <button type="submit" className="search-btn" onClick={handleSearch}><i className="fas fa-search"></i> </button>
        </form>
    )
