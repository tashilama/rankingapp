import React from 'react';

export default ({ data, postsPerPage, currentPageNumber, handlePaginate, isUserSearching, searchResults, }) => {

    let pageNumbers = [];
    let prevPageNumber = currentPageNumber - 1;
    let nextPageNumber = currentPageNumber + 1;

    let totalPosts = isUserSearching ? searchResults.length : data.length;

    for (let i = 1; i <= Math.ceil(totalPosts / postsPerPage); i++) {
        pageNumbers.push(i);
    }

    let NumberOfPageButtons = 5;

    let filteredPageNumbersToDisplay = pageNumbers.filter(pageNumber => {
        if ((currentPageNumber + NumberOfPageButtons) < pageNumbers.length) {
            if (pageNumber >= currentPageNumber && pageNumber < (currentPageNumber + NumberOfPageButtons))
                return true;
            else
                return false;
        }
        else {
            if (pageNumber <= pageNumbers.length && pageNumber >= (pageNumbers.length - NumberOfPageButtons))
                return true;
            else
                return false;
        }
    });

    //For active page number 
    let pageNumberBlocks = filteredPageNumbersToDisplay.map(pageNumber => {
        if (pageNumber === currentPageNumber)
            return (<span className="page-numbers active" onClick={() => handlePaginate(pageNumber)} key={pageNumber}> {pageNumber} </span>)
        else
            return (<span className="page-numbers" onClick={() => handlePaginate(pageNumber)} key={pageNumber}> {pageNumber} </span>)
    });

    //angle buttons
    let prevPageButton = prevPageNumber > 0 && (<span className="page-numbers" onClick={() => handlePaginate(prevPageNumber)}>
        <i className="fas fa-angle-left"></i>
    </span>)

    let nextPageButton = ((currentPageNumber + NumberOfPageButtons) < pageNumbers.length) && (<span className="page-numbers" onClick={() => handlePaginate(nextPageNumber)}>
        <i className="fas fa-angle-right"></i>
    </span>)

    return (
        <section className="pagination">
            {prevPageButton}
            {pageNumberBlocks}
            {nextPageButton}
        </section>
    );
}