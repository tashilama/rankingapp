import React from 'react';
import axios from 'axios';
//data
import { appTypesNotForCategory } from '../data/appTypes';
import { appTypesForCategory } from '../data/appTypes';
//component
import MainForm from './MainForm';
import LeaderBoard from './LeaderBoard';
import Pagination from './Pagination';
//validation
import isEmpty from '../validations/isEmpty';

class Home extends React.Component {

	state = {
		data: [],
		country: '',
		collection: appTypesNotForCategory[0].value,
		//category
		category: '',
		showCategoryField: false,
		//state for styling category toggle
		categoryStyles: {
			transform: "translateX(0px)",
			lineBackground: "#d1cccc",
			circleBackground: "#908e7f",
			boxShadow: "none"
		},
		loading: false,
		error: '',
		//states for pagination 
		currentPageNumber: 1,
		postsPerPage: 10,
		//search
		searchInput: "",
		isUserSearching: false,
		searchResults: []
	};

	//handler for React Select library
	handleReactSelect = fieldName => e => {
		this.setState({
			[fieldName]: e.value, 							//since custom select has no target attribute
		}, () => console.log("selected", this.state));
	};

	//handler for normal Select (app type), Text Input
	handleEvent = fieldName => e => {
		this.setState({
			[fieldName]: e.target.value,
		});
	};

	//handler to view category field
	handleShowCategoryField = () => {

		if (this.state.showCategoryField === false) {
			this.setState({
				data: [],									//emptying leaderboard

				category: '',

				showCategoryField: true,
				collection: appTypesForCategory[0].value,
				categoryStyles: {
					transform: "translateX(38px)",
					lineBackground: "#f5df26",
					circleBackground: "#e6ce00",			//yellow
					boxShadow: "0 0 20px #0000006b"
				},
				currentPageNumber: 1,
				//searchbox
				searchInput: "",
				isUserSearching: false,
				searchResults: [],
				error: ""
			}, () => console.log(this.state))
		}
		else {
			this.setState({
				//emptying fields
				data: [],

				category: '',

				showCategoryField: false,
				collection: appTypesNotForCategory[0].value,
				categoryStyles: {
					transform: "translateX(0px)",
					lineBackground: "#d1cccc",
					circleBackground: "#908e7f",			//silver
					boxShadow: "none"
				},
				currentPageNumber: 1,
				//searchbox
				searchInput: "",
				isUserSearching: false,
				searchResults: [],
				error: ""
			}, () => console.log(this.state))
		}


	}

	//handler for pagination
	handlePaginate = pageNumber => {
		this.setState({ currentPageNumber: pageNumber });
	}

	handleSubmit = e => {

		e.preventDefault();
		let APIendpoint = "";

		if (this.state.showCategoryField) {
			if ((!isEmpty(this.state.country)) && (!isEmpty(this.state.category))) {
				APIendpoint = `http://68.183.89.242:3000/api/apps?collection=${this.state.collection}&country=${this.state.country}&category=${this.state.category}`;
				this.makeRequestForAppData(APIendpoint);
			}
			else {
				alert("Fill up the fields");
			}
		} else {
			if ((!isEmpty(this.state.country))) {
				APIendpoint = `http://68.183.89.242:3000/api/apps?collection=${this.state.collection}&country=${this.state.country}`
				this.makeRequestForAppData(APIendpoint);
			}
			else {
				alert("Fill up the fields");
			}
		}

	};

	makeRequestForAppData = (APIendpoint) => {

		this.setState({
			loading: true,
			currentPageNumber: 1,		//so that on every submit, result starts from rank 1
			searchInput: "",
			isUserSearching: false,
			searchResults: [],
			error: ''
		});

		axios({
			method: "GET",
			url: APIendpoint,
		}).then(res => {
			console.log(res.data);
			this.setState({
				data: res.data.results,
				loading: false,
				error: ''
			});
		})
			.catch(err => {
				let errorMessage = err.message.includes("status code 400") ? "Request Invalid" : err.message;
				this.setState({
					loading: false,
					error: errorMessage,
					data: [],
				});
			});

	}

	//handleSearch
	handleSearch = e => {
		e.preventDefault();
		this.setState({
			isUserSearching: true,
			currentPageNumber: 1
		});

		let searchInput = this.state.searchInput;
		let filterSearchedData = [];
		if (!isEmpty(this.state.data)) {

			filterSearchedData = this.state.data.filter(app =>
				(app.title.toLowerCase().indexOf(searchInput.toLowerCase()) !== -1)
			);

			this.setState({
				searchResults: filterSearchedData
			}, () => console.log(this.state.searchResults));
		}

	}

	render() {

		return (
			<React.Fragment>
				<MainForm {...this.state}
					handleShowCategoryField={this.handleShowCategoryField}
					handleEvent={this.handleEvent}
					handleReactSelect={this.handleReactSelect}
					handleSubmit={this.handleSubmit} />
				<LeaderBoard {...this.state} handleEvent={this.handleEvent} handleSearch={this.handleSearch} />
				{(!isEmpty(this.state.data)) && <Pagination {...this.state} handlePaginate={this.handlePaginate} />}
			</React.Fragment>
		);
	}
}

export default Home;
