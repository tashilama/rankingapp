import React from 'react';
import Select from 'react-select';
//data
import countries from '../../data/country';
import categories from '../../data/categories';
import { appTypesNotForCategory } from '../../data/appTypes';
import { appTypesForCategory } from '../../data/appTypes';
//component
import ToggleCategory from '../ToggleCategory';

export default ({ loading, showCategoryField, handleShowCategoryField, categoryStyles, handleSubmit, handleReactSelect, handleEvent }) => {

    let newAppTypes = showCategoryField ? appTypesForCategory : appTypesNotForCategory;

    let appTypeOptions = newAppTypes.map((appType, i) => (
        <option value={appType.value} key={'appType_' + i}>
            {' '}{appType.label}{' '}
        </option>
    ))

    let categoryField = showCategoryField &&
        (<div id="category">
            <Select
                placeholder="Choose a category"
                className="basic-single"
                classNamePrefix="select"
                name="category"
                id="category"
                options={categories}
                onChange={handleReactSelect('category')}
                required
            />
        </div>)

    return (
        <section className="main-form-container">

            <ToggleCategory handleShowCategoryField={handleShowCategoryField} styles={categoryStyles} />

            <form className="option-selection-form" onSubmit={handleSubmit}>
                {/* <div className="form-input" id="mobile-os">
                    Google Play Store
                </div> */}
                <section className="selectors-container">
                    <div id="country">
                        <Select
                            placeholder="Choose a country"
                            className="basic-single"
                            classNamePrefix="select"
                            name="country"
                            id="country"
                            options={countries}
                            onChange={handleReactSelect('country')}
                            required
                        />
                    </div>

                    <div id="app-type">
                        <select className="app-type-form" onChange={handleEvent('collection')}>
                            {appTypeOptions}
                        </select>
                    </div>

                    {categoryField}
                </section>

                <button id={loading ? "go-btn-loading" : "go-btn"} type="submit">
                    <span className="btn-label">
                        GO <i className="fas fa-angle-double-right"></i>
                    </span>
                </button>
            </form>

        </section>

    )
}