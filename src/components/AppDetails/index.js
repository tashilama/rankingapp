import React from 'react';
import axios from 'axios';
import '../../css/styles.css';

class AppDetails extends React.Component {

    state = {
        app: null,
        error: ''
    }

    componentDidMount() {
        let appId = this.props.match.params.appId;

        axios.get('http://68.183.89.242:3000/api/apps/' + appId)
            .then(res => {
                console.log(res.data);
                this.setState({
                    app: res.data,
                    error: ''
                })
            })
            .catch(err => this.setState({ error: err.message }))
    }

    getMinInstallsData(a) {

        let newData = [];
        //format minInstalls
        let countDigit = 0;
        for (let i = a.length - 1; i >= 0; i--) {
            countDigit++;
            if (countDigit % 3 === 0)
                newData[i] = ',0'
            else
                newData[i] = "0"
        }
        newData[0] = a[0];
        return newData;
    }

    render() {
        let date = this.state.app ? new Date(this.state.app.updated).toLocaleDateString() : null;

        let minInstalls = this.state.app ? this.getMinInstallsData(this.state.app.minInstalls.toString()) : null;

        let appDetails = this.state.app ? (
            <>
                <div className="app-details-header">
                    <h1 className="app-title">{this.state.app.title}</h1>
                    <p className="app-summary">{this.state.app.summary}</p>
                </div>
                <div className="app-details-body">
                    <div className="app-header-img-container">
                        <img src={this.state.app.headerImage} alt="App header img" className="header-img" />
                    </div>
                    <div className="app-description">
                        <p className="description-section"> <span className="description-label"> Installs:  </span>{this.state.app.installs}</p>
                        <p className="description-section"> <span className="description-label"> Minimum Installs: </span>{minInstalls}</p>
                        <p className="description-section"> <span className="description-label"> Score:  </span>{this.state.app.scoreText}</p>
                        <p className="description-section"> <span className="description-label"> Initial Release:  </span>{this.state.app.released}</p>
                        <p className="description-section"> <span className="description-label"> Last Updated:  </span>{date}</p>
                    </div>
                    <a href={this.state.app.playstoreUrl} target='_blank' className="download-btn" rel="noopener noreferrer">
                        Download    <i className="fas fa-arrow-circle-down"></i>
                    </a>
                </div>
            </>
        ) : (
                <h3>{this.state.error}</h3>
            );

        return (
            <>
                <div className="app-details-container">
                    {appDetails}
                </div>
            </>
        );
    }
}

export default AppDetails;