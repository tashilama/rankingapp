import React from 'react';
import { Link } from 'react-router-dom';
import isEmpty from '../validations/isEmpty';
//component
import SearchBox from './SearchBox';

export default (props) => {

    let { data, isUserSearching, searchResults, loading, error, currentPageNumber, postsPerPage } = props;

    //logic to find index of first and last app from results displayed
    let indexOfLastPost = currentPageNumber * postsPerPage;
    let indexOfFirstPost = indexOfLastPost - postsPerPage;

    let dataToDisplay = isUserSearching ? searchResults : data;
    let currentPosts = dataToDisplay.slice(indexOfFirstPost, indexOfLastPost);

    //For error or non error messages
    let nonErrorMessage = isUserSearching ? "Your search did not match any record"
        : "select the options above and press GO"

    let messageBlock = error ? <div className="alert alert-danger"><i className="fas fa-exclamation-triangle"></i> {error}</div>
        : <div className="alert no-data">{nonErrorMessage} <i className="fas fa-exclamation"></i></div>

    let imageForMessage = isEmpty(error) && (isUserSearching) ?
        <img src={require("../assets/images/no_search_data.png")} alt="No search Data" className="no-data-img" />
        : <img src={require("../assets/images/no_data.png")} alt="No Data" className="no-data-img" />

    let tableContent = (
        <div className="no-content">
            {messageBlock}
            {imageForMessage}
        </div>
    );

    if ((!isEmpty(data)) && (!isEmpty(currentPosts))) {

        tableContent = currentPosts.map((result, i) => (
            <div className={`table-row record${i}`} key={'row_' + i}>

                <span className="row-data serial-no">
                    {data.indexOf(result) + 1}.
                </span>

                <span className="row-data title">
                    <a href={result.playstoreUrl} target="_blank" rel="noopener noreferrer" className="app-title-playstore-link">
                        {result.title}
                    </a>
                </span>
                <span className="row-data img">
                    <img src={result.icon} style={{ width: '50px', height: '50px' }} alt="app icon" />
                </span>
                <span className="row-data details">
                    <Link to={`/app-details/${result.appId}`} target="_blank" className="app-details-link">Details <i className="fas fa-external-link-alt"></i></Link>
                </span>
            </div>
        ))
    }

    let skeletonLoader = [];
    for (let i = 0; i < postsPerPage; i++) {
        skeletonLoader.push(<div className="table-row-loading" key={'key' + i}></div>)
    }

    //for leaderboard title
    let backgroundStyles = isEmpty(data) ?
        { backgroundPosition: 'center 0', color: '#333333', boxShadow: 'none' } :
        { backgroundPosition: 'center 95%', color: '#f5f5f5', boxShadow: ' 0 12px 8px -8px #04a6ab' };

    return (
        <>
            <div className="leaderboard">
                <div className="leaderboard-title-container">
                    <h1 className="leaderboard-title" style={{ ...backgroundStyles }}>
                        LEADERBOARD
                    </h1>
                </div>
                <SearchBox {...props} />
                <div id="table-container">
                    <section id="table-title">
                        <span className="row-data title">S.N.</span>
                        <span className="row-data title">Title</span>
                        <span className="row-data title">Icon</span>
                        <span className="row-data title">Details</span>
                    </section>
                    {loading ? skeletonLoader : tableContent}
                </div>

            </div>
        </>
    )
}