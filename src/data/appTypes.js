export const appTypesNotForCategory = [
	{ value: 'topselling_free', label: 'Top Free' },
	{ value: 'topselling_paid', label: 'Top Paid' },
	//{ value: 'topselling_new_free', label: 'New Free' },
	//{ value: 'topselling_new_paid', label: 'New Paid' },
	{ value: 'topgrossing', label: 'Top Grossing' },
	//{ value: 'movers_shakers', label: 'Trending' },
	{ value: 'topselling_free_games', label: 'Top Free Games' },
	{ value: 'topselling_paid_games', label: 'Top Paid Games' },
	{ value: 'topselling_grossing_games', label: 'Top Grossing Games' },
];

export const appTypesForCategory = [
	{ value: 'topselling_free', label: 'Top Free' },
	{ value: 'topselling_paid', label: 'Top Paid' },
	{ value: 'topselling_new_free', label: 'New Free' },
	{ value: 'topselling_new_paid', label: 'New Paid' },
	{ value: 'topgrossing', label: 'Top Grossing' },
	{ value: 'movers_shakers', label: 'Trending' },
	{ value: 'topselling_free_games', label: 'Top Free Games' },
	{ value: 'topselling_paid_games', label: 'Top Paid Games' },
	{ value: 'topselling_grossing_games', label: 'Top Grossing Games' },
];